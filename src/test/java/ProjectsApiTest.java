import org.junit.Assert;
import org.junit.Test;

import io.swagger.client.ApiException;
import io.swagger.client.api.ProjectsApi;
import io.swagger.client.model.Project;
import io.swagger.client.model.Projects;

public class ProjectsApiTest extends BaseApiTest {

	@Test
	public void getAllProjects() throws ApiException {

		ProjectsApi projectsApi = new ProjectsApi();
		Projects allProjects = projectsApi.apiV3ProjectsGet(null);
		Assert.assertTrue(allProjects.getCount() > 3);
		Assert.assertEquals(allProjects.getCount(), new Integer(allProjects.getEmbedded().getElements().size()));
	}

	@Test
	public void getOneProject() throws ApiException {

		ProjectsApi projectsApi = new ProjectsApi();
		Project eko = projectsApi.apiV3ProjectsIdGet(18);
		Assert.assertNotNull(eko);
	}

}
