import org.junit.Assert;
import org.junit.Test;

import io.swagger.client.ApiException;
import io.swagger.client.api.VersionsApi;
import io.swagger.client.model.Version;
import io.swagger.client.model.Versions;

public class VersionApiTest extends BaseApiTest {

	@Test
	public void getAllVersions() throws ApiException {

		VersionsApi versionsApi = new VersionsApi();
		Versions allVersions = versionsApi.apiV3VersionsGet(null);
		Assert.assertTrue(allVersions.getCount() > 3);
		Assert.assertEquals(allVersions.getCount(), new Integer(allVersions.getEmbedded().getElements().size()));
	}

	@Test
	public void getOneVersion() throws ApiException {

		VersionsApi versionsApi = new VersionsApi();
		Version version = versionsApi.apiV3VersionsIdGet(9);
		Assert.assertNotNull(version);
	}

}
