import org.junit.Before;

import io.swagger.client.ApiClient;
import io.swagger.client.Configuration;

public class BaseApiTest {

	private static final String APIKEY = System.getProperty("apiKey");
	private static final String USERNAME = "apikey";

	@Before
	public void configureApiAccess() {
		ApiClient apiClient = new ApiClient();
		apiClient.setBasePath("http://spmlga.kaneo.corp");
		apiClient.setDebugging(true);
		if (APIKEY == null || APIKEY.isEmpty()) {
			throw new IllegalArgumentException("API Key missing - pass it as parameter on start [ -DapiKey=foobar ]");
		}
		apiClient.setUsername(USERNAME);
		apiClient.setPassword(APIKEY);

		Configuration.setDefaultApiClient(apiClient);
	}
}
