import org.junit.Test;

import io.swagger.client.ApiException;
import io.swagger.client.api.UsersApi;
import io.swagger.client.model.User;

public class ConnectionTest extends BaseApiTest {

	@Test
	public void test() throws ApiException {
		UsersApi usersApi = new UsersApi();
		User me = usersApi.apiV3UsersIdGet("me");
		System.out.println(me.getLogin());
	}

}
