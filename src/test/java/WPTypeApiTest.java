import org.junit.Assert;
import org.junit.Test;

import io.swagger.client.ApiException;
import io.swagger.client.api.TypesApi;
import io.swagger.client.model.WPType;
import io.swagger.client.model.WPTypes;

public class WPTypeApiTest extends BaseApiTest {

	@Test
	public void getAllVersions() throws ApiException {

		TypesApi typesApi = new TypesApi();
		WPTypes allTypes = typesApi.apiV3TypesGet();
		Assert.assertTrue(allTypes.getCount() > 3);
		Assert.assertEquals(allTypes.getCount(), new Integer(allTypes.getEmbedded().getElements().size()));
	}

	@Test
	public void getOneWPType() throws ApiException {

		TypesApi typesApi = new TypesApi();
		WPType type = typesApi.apiV3TypesIdGet(1);
		Assert.assertNotNull(type);
	}

}
