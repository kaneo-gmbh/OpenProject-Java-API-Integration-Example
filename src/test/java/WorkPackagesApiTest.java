import org.junit.Assert;
import org.junit.Test;

import io.swagger.client.ApiException;
import io.swagger.client.api.TypesApi;
import io.swagger.client.api.WorkPackagesApi;
import io.swagger.client.model.Description;
import io.swagger.client.model.WPType;
import io.swagger.client.model.WorkPackage;
import io.swagger.client.model.WorkPackageLinks;
import io.swagger.client.model.WorkPackagePatch;
import io.swagger.client.model.WorkPackages;

public class WorkPackagesApiTest extends BaseApiTest {

	@Test
	public void getSomeWorkpackages() throws ApiException {

		WorkPackagesApi wpApi = new WorkPackagesApi();
		String filters = "[{\"createdAt\": {\"operator\": \">t-\" ,\"type\": \"date_past\", \"values\":[\"30\"]}}]";
		WorkPackages workPackages = wpApi.apiV3ProjectsIdWorkPackagesGet(18, 1, 10, filters, null, null, false);
		Assert.assertTrue(workPackages.getCount() > 3);
		Assert.assertEquals(workPackages.getCount(), new Integer(workPackages.getEmbedded().getElements().size()));

	}

	@Test
	public void getOneWorkpackage() throws ApiException {

		WorkPackagesApi wpApi = new WorkPackagesApi();
		WorkPackage workPackage = wpApi.apiV3WorkPackagesIdGet(555);
		Assert.assertNotNull(workPackage);
	}

	@Test
	public void createAndDeleteWP() throws ApiException {

		TypesApi typesApi = new TypesApi();
		WPType typePhase = typesApi.apiV3TypesIdGet(3);

		WorkPackagesApi wpApi = new WorkPackagesApi();
		Integer nuclosKaneoProjektId = 11;
		WorkPackage wp = new WorkPackage();
		Description description = new Description();
		description.setRaw("autogenerier via API");
		wp.setDescription(description);
		wp.setSubject("Test Ticket");
		WorkPackageLinks links = new WorkPackageLinks();
		links.setType(typePhase.getLinks().getSelf());
		//		links.setParent(parent);
		wp.setLinks(links);

		WorkPackage newWp = wpApi.apiV3ProjectsIdWorkPackagesPost(nuclosKaneoProjektId, wp, false);
		Assert.assertNotNull(newWp);
		Assert.assertEquals(wp.getSubject(), newWp.getSubject());
		Assert.assertEquals(wp.getDescription().getRaw(), newWp.getDescription().getRaw());
		Assert.assertEquals(wp.getLinks().getType().getHref(), newWp.getLinks().getType().getHref());

		//change status
		WorkPackagePatch patch = new WorkPackagePatch();
		patch.setLockVersion(newWp.getLockVersion());
		patch.setSubject("ge�ndert");
		WorkPackage patched = wpApi.apiV3WorkPackagesIdPatch(newWp.getId(), false, patch);
		Assert.assertEquals(patch.getSubject(), patched.getSubject());
		Assert.assertEquals(newWp.getDescription().getRaw(), patched.getDescription().getRaw());

		wpApi.apiV3WorkPackagesIdDelete(newWp.getId());

		try {
			wpApi.apiV3WorkPackagesIdGet(newWp.getId());
			Assert.fail("Es sollte vorher knallen");
		} catch (ApiException e) {
			Assert.assertEquals("Not Found", e.getMessage());
		}
	}

}
