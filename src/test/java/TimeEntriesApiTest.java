import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import io.swagger.client.ApiException;
import io.swagger.client.api.TimeEntriesApi;
import io.swagger.client.api.UsersApi;
import io.swagger.client.model.Link;
import io.swagger.client.model.TimeEntries;
import io.swagger.client.model.TimeEntry;
import io.swagger.client.model.User;

public class TimeEntriesApiTest extends BaseApiTest {

	@Test
	public void getOneTimeEntry() throws ApiException {

		TimeEntriesApi timeApi = new TimeEntriesApi();
		TimeEntry timeEntry = timeApi.apiV3TimeEntriesIdGet(55);
		//		WorkPackage workPackage = timeEntry.getEmbedded().getWorkPackage();
		//		Assert.assertNotNull(workPackage);
		//		Assert.assertNotNull(workPackage.getId());
	}

	@Test
	public void getTimeEntriesByUser() throws ApiException {

		UsersApi usersApi = new UsersApi();
		User me = usersApi.apiV3UsersIdGet("me");

		TimeEntriesApi timeApi = new TimeEntriesApi();
		//		String filters = "[]";
		String filters = "[{\"user\": {\"operator\": \"=\" , \"values\":[\"" + me.getId() + "\"]}}]";
		TimeEntries timeEntries = timeApi.apiV3TimeEntriesGet(null, 10, filters);
		List<TimeEntry> elements = timeEntries.getEmbedded().getElements();
		for (TimeEntry timeEntry : elements) {
			// check user
			Link link2User = timeEntry.getLinks().getUser();
			String href = link2User.getHref();
			try {
				String id = href.substring(href.lastIndexOf('/') + 1, href.length());
				Assert.assertEquals(me.getId(), new Integer(id));
			} catch (IndexOutOfBoundsException | NumberFormatException e) {
				Assert.fail("der User Link beinhaltet einen invaliden Link");
			}

		}
		Assert.assertNotNull(elements);
		Assert.assertTrue(elements.size() > 5);
	}

}
